import { setupCascadingAbort } from "abort-tools";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import { createIterableFanout } from "iterable-fanout";
import defer from "p-defer";
import { createRetryIterable } from "retry-iterable";

export const queryError = Symbol();
export type QueryError = typeof queryError;

export const defaultQueryMemoizerOptions = {
    resolve(queryElement?: QueryElement<unknown, unknown> | QueryError) {
        return queryElement != null;
    },

    key(...args: unknown[]) {
        return JSON.stringify(args);
    },

    onError(error: unknown) {
        throw error;
    },
};

export interface QueryMemoizerOptions<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    S = any, E = any, A extends unknown[] = any[]
> {
    initialState: S,
    reduce: (state: S, event: E) => S,
    source: (signal: AbortSignal, ...args: A) => AsyncIterable<E>,

    resolve?: (queryElement?: QueryElement<S, E> | QueryError) => boolean,
    key?: (...args: A) => string,
    onError?: (error: unknown) => void

    retryIntervalBase: number,
    retryIntervalCap: number,
}

export type QueryMemoizer<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    S = any, E = any, A extends unknown[] = any[]
> = InstanceMemoizer<
    Promise<Query<S, E>>, A
>

export function createQueryMemoizer<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    S = any, E = any, A extends unknown[] = any[]
>(
    options: QueryMemoizerOptions<S, E, A>,
): QueryMemoizer<S, E, A> {
    const {
        initialState, reduce,
        resolve, key, onError,
        source,
        retryIntervalBase, retryIntervalCap,
    } = {
        ...defaultQueryMemoizerOptions,
        ...options,
    };

    const abortControllers = new WeakMap<
        Promise<Query<S, E | QueryError>>,
        AbortController
    >();

    const createInstance = (...args: A) => {
        const abortController = new AbortController();
        const instance = instantiate(
            abortController.signal,
            ...args,
        );
        abortControllers.set(instance, abortController);
        abortController.signal.addEventListener(
            "abort",
            () => abortControllers.delete(instance),
        );
        return instance;
    };

    const destroyInstance = (
        instance: Promise<Query<S, E>>,
    ) => {
        const abortController = abortControllers.get(instance);
        assert(abortController);
        abortController.abort();
    };

    const memoizer = new InstanceMemoizer(
        createInstance,
        destroyInstance,
        key,
    );
    return memoizer;

    async function instantiate(
        signal: AbortSignal,
        ...args: A
    ) {
        const deferred = defer<void>();
        let resolved = resolve();
        if (resolved) {
            deferred.resolve();
        }

        let state = initialState;

        const queryElements = createRetryIterable<QueryElement<S, E> | QueryError>({
            signal,
            retryIntervalBase, retryIntervalCap,
            async * factory() {
                // use a cascading abort here so we can cancel the source
                // in case of an error! Especially important when there is
                // an error in the reducer.
                const abortController = new AbortController();
                setupCascadingAbort(signal, abortController);
                try {
                    const elements = source(
                        abortController.signal,
                        ...args,
                    );
                    const queryElements = reduceEvents(elements);
                    yield* queryElements;
                }
                finally {
                    abortController.abort();
                }
            },
            * mapError(error) {
                // if the signal is aborted we are done with this query, it is
                // is very normal that an AbortError is generated. But even when
                // an other error is generated, we don't care because we don't
                // need the query anyway.
                if (signal.aborted) return;

                onError(error);

                yield queryError;

                memoizer.flushInstance(...args);
            },
        });

        const resolvedElements = resolveElements(queryElements);
        const assertedElements = assertElements(resolvedElements);
        const fanout = createIterableFanout(assertedElements);
        fanout.finished.catch(error => {
            if (signal.aborted) {
                return;
            }
            throw error;
        });

        const fork = fanout.fork;
        const getState = () => state;

        const query: Query<S, E> = {
            getState,
            fork,
        };

        await deferred.promise;

        return query;

        async function* assertElements<T>(elements: AsyncIterable<T>) {
            yield* elements;
            // a query may only end when the signal is aborted! If this is
            // not the case we are dealing with a serious bug. This assertion
            // would expose that bug.
            assert(signal.aborted, "expected aborted signal");
        }

        async function* resolveElements(
            elements: AsyncIterable<QueryElement<S, E> | QueryError>,
        ) {
            for await (const element of elements) {
                if (element !== queryError) {
                    yield element;
                }

                if (!resolved && resolve(element)) {
                    deferred.resolve();
                    resolved = true;
                }
            }
        }

        async function* reduceEvents(
            events: AsyncIterable<E>,
        ): AsyncIterable<QueryElement<S, E>> {
            for await (const event of events) {
                state = reduce(state, event);

                yield { state, event };
            }
        }

    }
}

export async function getQueryState<
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    S = any, E = any, A extends unknown[] = any[]
>(
    memoizer: QueryMemoizer<S, E, A>,
    linger: number,
    ...args: A
): Promise<S> {
    const queryPromise = memoizer.acquire(...args);
    try {
        const query = await queryPromise;
        const state = query.getState();
        return state;
    }
    finally {
        memoizer.release(queryPromise, linger);
    }
}

export interface QueryElement<S, E> {
    state: S
    event: E
}

export interface Query<S, E> {
    fork(signal?: AbortSignal): AsyncIterable<QueryElement<S, E>>;
    getState(): S;
}

export async function* mapQueryEvents<E>(
    iterable: AsyncIterable<QueryElement<unknown, E>>,
): AsyncIterable<E> {
    for await (const element of iterable) {
        yield element.event;
    }
}
