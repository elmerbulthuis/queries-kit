import { AbortError, isAbortError } from "abort-tools";
import assert from "assert";
import { BufferedIterable, createBufferedIterable } from "buffered-iterable";
import test from "tape-promise/tape.js";
import { createLeadFollowEvents } from "./lead-follow-events.js";
import { createQueryMemoizer } from "./query-memoizer.js";
import { waitFor } from "./wait.js";

const errorSymbol = Symbol();

class TestContext {
    private abortController = new AbortController();

    leadIterableMaybe: BufferedIterable<number> | undefined;
    followIterables = new Map<number, BufferedIterable<number>>();

    waitForLeadIterable = () => waitFor(() => {
        const iterable = this.leadIterableMaybe;
        assert(iterable != null);
        return iterable;
    });

    waitForFollowIterable = (key: number) => waitFor(() => {
        const iterable = this.followIterables.get(key);
        assert(iterable != null);
        return iterable;
    });

    leadQueryMemoizer = createQueryMemoizer({
        initialState: 0,
        reduce(state, event: number) {
            return event;
        },
        source: signal => {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const self = this;
            return {
                async *[Symbol.asyncIterator]() {
                    const iterable = createBufferedIterable<number>();
                    const onAbort = () => iterable.error(new AbortError("aborted"));

                    self.leadIterableMaybe = iterable;
                    signal.addEventListener(
                        "abort",
                        onAbort,
                    );
                    try {
                        yield* iterable;
                    }
                    finally {
                        self.leadIterableMaybe = undefined;
                        signal.removeEventListener(
                            "abort",
                            onAbort,
                        );
                    }
                },
            };
        },
        retryIntervalBase: 0,
        retryIntervalCap: 0,
        onError: error => {
            if (error !== errorSymbol) {
                throw error;
            }
        },
    });

    followQueryMemoizer = createQueryMemoizer({
        initialState: 0,
        reduce(state, event: number) {
            return event;
        },
        source: (signal, id: number) => {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const self = this;
            return {
                async *[Symbol.asyncIterator]() {
                    const iterable = createBufferedIterable<number>();
                    const onAbort = () => iterable.error(new AbortError("aborted"));

                    self.followIterables.set(id, iterable);
                    signal.addEventListener(
                        "abort",
                        onAbort,
                    );
                    try {
                        yield* iterable;
                    }
                    finally {
                        self.followIterables.delete(id);
                        signal.removeEventListener(
                            "abort",
                            onAbort,
                        );
                    }
                },
            };
        },
        retryIntervalBase: 0,
        retryIntervalCap: 0,
        onError: error => {
            if (error !== errorSymbol) {
                throw error;
            }
        },
    });

    eventsIterable = createLeadFollowEvents({
        signal: this.abortController.signal,
        leadQueryFactory: this.leadQueryMemoizer,
        followQueryFactory: this.followQueryMemoizer,
        getFollowKeys: function* (
            leadState: number,
        ): Iterable<number> {
            yield leadState;
        },
        getFollowArgs: function (
            leadState: number,
            key: number,
        ): readonly [number] {
            return [key];
        },
        mapLeadSnapshot: function* (
            leadState: number,
            followStates: Record<number, number>,
        ): Iterable<number> {
            yield* Object.values(followStates);
        },
        mapFollowSnapshot: function* (
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            followKey: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            followState: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            leadState: number,
        ): Iterable<number> {
            yield* [];
        },
        mapLeadEvent: function* (
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            leadEvent: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            leadStateNext: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            leadStatePrev: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            followStates: Record<number, number>,
        ): Iterable<number> {
            yield* [];
        },
        mapFollowEvent: function* (
            followEvent: number,
            followKey: number,
            followStateNext: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            followStatePrev: number,
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            leadState: number,
        ): Iterable<number> {
            yield followStateNext;
        },
    }, []);

    abort() {
        this.abortController.abort();
    }

    static async with<T>(
        task: (testContext: TestContext) => Promise<T>,
    ) {
        const testContext = new TestContext();
        const result = await task(testContext);
        return result;
    }
}

test(
    "lead-follow",
    t => TestContext.with(async testContext => {
        const task = async () => {
            const leadIterable = await testContext.waitForLeadIterable();
            leadIterable.push(1);

            const followIterable = await testContext.waitForFollowIterable(1);
            followIterable.push(2);
        };

        const taskPromise = task();

        const eventsIterator = testContext.eventsIterable[Symbol.asyncIterator]();

        const eventsNext = await eventsIterator.next();
        assert(eventsNext.done !== true);

        t.strictEqual(eventsNext.value, 2);

        testContext.abort();

        try {
            await eventsIterator.next();
            t.fail();
        }
        catch (error) {
            t.ok(isAbortError(error));
        }

        await taskPromise;

        t.strictEqual(testContext.leadQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.leadQueryMemoizer.instanceTotal, 0);

        t.strictEqual(testContext.followQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.followQueryMemoizer.instanceTotal, 0);
    }),
);

test(
    "lead-follow with error",
    t => TestContext.with(async testContext => {
        const task = async () => {
            const leadIterable = await testContext.waitForLeadIterable();
            leadIterable.error(errorSymbol);

            const followIterable = await testContext.waitForFollowIterable(0);
            followIterable.push(2);
        };

        const taskPromise = task();

        const eventsIterator = testContext.eventsIterable[Symbol.asyncIterator]();

        const eventsNext = await eventsIterator.next();
        assert(eventsNext.done !== true);

        t.strictEqual(eventsNext.value, 2);

        testContext.abort();

        try {
            await eventsIterator.next();
            t.fail();
        }
        catch (error) {
            t.ok(isAbortError(error));
        }

        await taskPromise;

        t.strictEqual(testContext.leadQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.leadQueryMemoizer.instanceTotal, 0);

        t.strictEqual(testContext.followQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.followQueryMemoizer.instanceTotal, 0);
    }),
);

test(
    "lead-follow with immediate abort",
    t => TestContext.with(async testContext => {
        const eventsIterator = testContext.eventsIterable[Symbol.asyncIterator]();

        testContext.abort();

        try {
            await eventsIterator.next();
            t.fail();
        }
        catch (error) {
            t.ok(isAbortError(error));
        }

        t.strictEqual(testContext.leadQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.leadQueryMemoizer.instanceTotal, 0);

        t.strictEqual(testContext.followQueryMemoizer.refTotal, 0);
        t.strictEqual(testContext.followQueryMemoizer.instanceTotal, 0);
    }),
);
