import { assertAbortSignal, setupCascadingAbort } from "abort-tools";
import { createBufferedIterable } from "buffered-iterable";
import { Query, QueryElement, QueryMemoizer } from "./query-memoizer.js";

export interface JoinEventsConfig<
    E,
    F extends QueryMemoizer[],
> {
    /**
     * Signal to abort the iterable and thereby release all the acquired queries.
     */
    signal: AbortSignal
    /**
     * Query factories that generate the queries we want to join
     */
    queryFactories: Readonly<F>,
    /**
     * Expected to create a snapshot event. This function is only called once
     * in the lifetime of the iterable
     */
    mapSnapshot: MapSnapshot<E, F>
    /**
     * Called for every event emitted by any query.
     */
    mapEvents: MapEvents<E, F>
}
/**
 * This function creates a join iterable, one important use case of this is to
 * create something comparable to an INNER JOIN in a relational database.
 * 
 * The queryFactories are the queries that we want to join. Those queries will be 
 * acquired with the arguments we provide in the args parameter.
 * 
 * When the join iterable is created the queries are first acquired with the giver
 * arguments. Then the queries are resolved. After resolving, the results of the
 * mapSnapshot function are emitted. This function is expected to yield a snapshot
 * event so the consumer can do a first initialization of it's state. Often, in case
 * of a join query this is a custom event that includes data from all of the queries
 * state.
 * 
 * Then all of the queries are iterated asynchronously and in parallel. For every
 * in every query the corresponding mapEvents function is called. This function is
 * expected to yield an event that increments a previously emitted event, so it can
 * be used in a reducer by the consumer. In case of a INNER JOIN equivalent this is
 * often an event based on the state of the two queries we want to join and a
 * comparison with a previous state.
 * 
 * Sometimes we want to re-emit the snapshot, like when we reach an unimplemented or
 * undefined state. Usually we throw an error so that the retry logic of the queryMemoizer
 * that is managing this iterable will recreate it and so re-emitting the snapshot and
 * events.
 * 
 * @param config configuration for this join iterable
 * @param args arguments used to acquire queries
 */
export async function* createJoinEvents<
    E,
    F extends QueryMemoizer[],
>(
    config: JoinEventsConfig<E, F>,
    ...args: ExtractArgs<F>
): AsyncIterable<E> {

    const {
        signal,
        queryFactories,
        mapSnapshot,
        mapEvents,
    } = config;

    const queryPromises = queryFactories.map(
        // eslint-disable-next-line security/detect-object-injection
        (queryFactory, index) => queryFactory.acquire(...args[index]),
    );

    const abortController = new AbortController();
    setupCascadingAbort(signal, abortController);

    try {
        /*
        setup vars
        */
        let iterables: AsyncIterable<QueryElement<unknown, unknown>>[];
        let states: unknown[];

        /*
        Phase one, the snapshot
        */
        {
            const resolved = await Promise.all(
                queryPromises.map(
                    queryPromise => resolveQuery(queryPromise, abortController.signal),
                ),
            );

            states = resolved.map(([state]) => state);
            iterables = resolved.map(([, iterable]) => iterable);

            for (const event of mapSnapshot(states as ExtractStates<F>)) {
                yield event;
            }
        }

        assertAbortSignal(signal, "join aborted");

        /*
        Phase two, events
        */
        {
            const sink = createBufferedIterable<E>();

            const readQuery = async (index: number) => {
                // eslint-disable-next-line security/detect-object-injection
                const mapEvent = mapEvents[index];
                // eslint-disable-next-line security/detect-object-injection
                const iterable = iterables[index];
                try {
                    for await (
                        const { event: queryEvent, state: queryStateNext } of iterable
                    ) {
                        for (const event of mapEvent(
                            queryEvent, queryStateNext, states,
                        )) {
                            sink.push(event);
                        }

                        // eslint-disable-next-line security/detect-object-injection
                        states[index] = queryStateNext;
                    }
                }
                catch (error) {
                    if (!abortController.signal.aborted) throw error;
                }

                sink.done();
            };

            iterables.map((iterable, index) => {
                readQuery(index).
                    catch(error => sink.error(error));
            });

            yield* sink;

            assertAbortSignal(signal, "join aborted");
        }
        /*
        */

    }
    finally {
        abortController.abort();

        queryFactories.map(
            // eslint-disable-next-line security/detect-object-injection
            (queryFactory, index) => queryFactory.release(queryPromises[index]),
        );
    }
}

async function resolveQuery<S, E>(
    queryPromise: Promise<Query<S, E>>,
    signal: AbortSignal,
) {
    const query = await queryPromise;
    const state = query.getState();
    const eventIterable = query.fork(signal);
    return [state, eventIterable] as const;
}

type MapSnapshot<
    E,
    F extends QueryMemoizer[]
> = (
    states: ExtractStates<F>
) => Iterable<E>;

type MapEvents<
    E,
    F extends QueryMemoizer[]
> = {
        readonly [P in keyof F]: (
            event: ExtractEvent<F[P]>,
            stateNext: ExtractState<F[P]>,
            states: ExtractStates<F>,
        ) => Iterable<E>
    }

type ExtractState<F> =
    F extends QueryMemoizer<infer S> ? S : never
type ExtractEvent<F> =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    F extends QueryMemoizer<any, infer E> ? E : never
type ExtractArg<F> =
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    F extends QueryMemoizer<any, any, infer A> ? A : never

type ExtractStates<
    F extends QueryMemoizer[]
> = {
        [P in keyof F]: ExtractState<F[P]>
    }
// eslint-disable-next-line @typescript-eslint/no-unused-vars
type ExtractEvents<
    F extends QueryMemoizer[]
> = {
        [P in keyof F]: ExtractEvent<F[P]>
    }
type ExtractArgs<
    F extends QueryMemoizer[]
> = {
        [P in keyof F]: ExtractArg<F[P]>
    }
