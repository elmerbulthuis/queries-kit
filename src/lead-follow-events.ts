import { assertAbortSignal, setupCascadingAbort, waitForAbort } from "abort-tools";
import assert from "assert";
import { createBufferedIterable } from "buffered-iterable";
import { InstanceMemoizer } from "instance-memoizer";
import { Query, QueryElement } from "./query-memoizer.js";

export interface LeadFollowEventsConfig<
    E, FollowKey extends string | number,
    LeadState, LeadEvent, LeadArgs extends Array<unknown>,
    FollowState, FollowEvent, FollowArgs extends Array<unknown>,
> {
    /*
    We need a signal here to abort this query.
    the lead-follow query is responsible to create both the lead and follow queries. Therefore
    it will also need a signal to abort the lead (and follow) queries.
    */
    signal: AbortSignal,

    leadQueryFactory: InstanceMemoizer<
        Promise<Query<LeadState, LeadEvent>>,
        LeadArgs
    >;
    followQueryFactory: InstanceMemoizer<
        Promise<Query<FollowState, FollowEvent>>,
        FollowArgs
    >;

    getFollowKeys(leadState: LeadState): Iterable<FollowKey>;
    getFollowArgs(leadState: LeadState, key: FollowKey): Readonly<FollowArgs>;

    /*
    Called when the lead (including all follows) is initialized and expected to emit a snapshot
    event. This function is called only once in the lifetime of the iterable.
    */
    mapLeadSnapshot(
        leadState: LeadState,
        followStates: Record<FollowKey, FollowState>
    ): Iterable<E>;
    /*
    Called when a follow query is initialized.
    */
    mapFollowSnapshot(
        followKey: FollowKey,
        followState: FollowState,
        leadState: LeadState
    ): Iterable<E>;

    /*
    called for every event of the lead query
    */
    mapLeadEvent(
        leadEvent: LeadEvent,
        leadStateNext: LeadState, leadStatePrev: LeadState,
        followStates: Record<FollowKey, FollowState>
    ): Iterable<E>;
    /*
    called for every event of the follow query
    */
    mapFollowEvent(
        followEvent: FollowEvent,
        followKey: FollowKey,
        followStateNext: FollowState, followStatePrev: FollowState,
        leadState: LeadState,
    ): Iterable<E>;
}
/**
 * 
 * Create a lead-follow iterable that is able to create and maintain, follow queries from
 * another, lead query. This is useful for creating a relational's database's LEFT JOIN
 * equivalent.
 * 
 * The leadQueryFactory will create a lead query based on the arguments passed. Then the
 * followQueryFactory will create follow queries based on the result of the getFollowKeys
 * and getFollowArgs functions.
 * 
 * First the lead query is created via the leadQueryFactory and with the arguments passed
 * in the args parameter. This query is resolved and then via the followQueryFactory and
 * arguments returned from the getFollowArgs the initial follow queries are created and
 * resolved. Then the snapshot event is emitted via the mapLeadSnapshot function. This is
 * the only time in the lifetime of the iterable this function is called.
 * 
 * Then for every event in the lead query, the follow queries are synchronized. Keys for
 * required queries are acquired via the getFollowKeys function. These are compared to the
 * keys of the current follow queries. For the keys that are not available as current yet,
 * follow queries are created via the followQueryFactory and the getFollowArgs function. If
 * a query is current but not required, then the query is aborted and released.
 * 
 * If the query comes in an undefined or unimplemented state it is common to throw an error.
 * The managing queryMemoizer will then recreate the iterable and so re-emitting the snapshot
 * event so we are stable again.
 * 
 * When implementing a LEFT JOIN-like iterable we might run into a situation that could be
 * considered undefined. If a follow snapshot event is emitted and there are entities in the
 * state, we have a weird situation. One would expect a new follow query to have no entities
 * available. In this case it is easiest to throw an error and so causing the managing query
 * to recreate the iterable. But the best solution for this is implementation specific. One
 * could also choose to emit a few events that would add the entities. One could also create
 * a specific event for this situation. The best solution is up the the situation.
 * 
 * Also there are quite a few other cases thinkable, like removing a follow query that has
 * entities in the state, usually treating this as an undefined situation and thereby throwing
 * an error is the best situation. Other solutions could be better but would probably also
 * complicate things.
 * 
 * Lead-follow iterables can be incredibly powerful and dramatically simplify your program, but
 * the iterable itself is quite complex to implement. Feel free to reach out to us if you need
 * help with your lead-follow iterable!
 * 
 * @param config configuration for this lead-follow iterable
 * @param args arguments used to acquire the lead query
 */
export async function* createLeadFollowEvents<
    E, FollowKey extends string | number,
    LeadState, LeadEvent, LeadArgs extends Array<unknown>,
    FollowState, FollowEvent, FollowArgs extends Array<unknown>,
>(
    config: LeadFollowEventsConfig<
        E, FollowKey,
        LeadState, LeadEvent, LeadArgs,
        FollowState, FollowEvent, FollowArgs
    >,
    args: LeadArgs,
): AsyncIterable<E> {
    const {
        signal,
        getFollowKeys, getFollowArgs,
        leadQueryFactory, followQueryFactory,
        mapLeadSnapshot, mapFollowSnapshot,
        mapLeadEvent, mapFollowEvent,
    } = config;

    const leadQueryPromise = leadQueryFactory.acquire(...args);
    const leadAbortController = new AbortController();
    /*
    when the signal is aborted, also abort the leadSignal
    */
    setupCascadingAbort(signal, leadAbortController);

    const followQueryPromises = {} as Record<
        FollowKey,
        Promise<Query<FollowState, FollowEvent>>
    >;
    const followAbortControllers = {} as Record<
        FollowKey,
        AbortController
    >;

    try {

        //#region helpers

        const initializeState = async () => {
            const followKeysToSpawn = Array.from(new Set(getFollowKeys(leadState)));
            for (const followKey of followKeysToSpawn) {
                assert(!(followKey in followAbortControllers));
                assert(!(followKey in followQueryPromises));

                const followAbortController = new AbortController();
                setupCascadingAbort(signal, followAbortController);
                const followArgs = getFollowArgs(leadState, followKey);
                const followQueryPromise = followQueryFactory.acquire(...followArgs);

                // eslint-disable-next-line security/detect-object-injection
                followAbortControllers[followKey] = followAbortController;
                // eslint-disable-next-line security/detect-object-injection
                followQueryPromises[followKey] = followQueryPromise;
            }
            await Promise.all(followKeysToSpawn.map(resolveFollowQuery));
        };

        const resolveFollowQuery = async (followKey: FollowKey) => {
            // eslint-disable-next-line security/detect-object-injection
            const followQueryPromise = followQueryPromises[followKey];
            // eslint-disable-next-line security/detect-object-injection
            const followAbortController = followAbortControllers[followKey];

            const followQuery = await Promise.race([
                followQueryPromise,
                waitForAbort(followAbortController.signal, "lead-follow aborted"),
            ]);
            const followState = followQuery.getState();
            const followEventIterable = followQuery.fork(followAbortController.signal);

            assert(!(followKey in followStates));
            assert(!(followKey in followEventIterables));

            followStates = { ...followStates };

            // eslint-disable-next-line security/detect-object-injection
            followStates[followKey] = followState;
            // eslint-disable-next-line security/detect-object-injection
            followEventIterables[followKey] = followEventIterable;

            // eslint-disable-next-line security/detect-object-injection
            followAbortController.signal.addEventListener(
                "abort",
                () => {
                    assert(followKey in followStates);
                    assert(followKey in followEventIterables);

                    followStates = { ...followStates };

                    // eslint-disable-next-line security/detect-object-injection
                    delete followStates[followKey];
                    // eslint-disable-next-line security/detect-object-injection
                    delete followEventIterables[followKey];
                },
            );

            return followQuery;
        };

        const emitSnapshotFromLeadQuery = () => {
            for (const event of mapLeadSnapshot(
                leadState,
                followStates,
            )) {
                sink.push(event);
            }
        };

        const emitSnapshotFromFollowQuery = (followKey: FollowKey) => {
            for (const event of mapFollowSnapshot(
                followKey,
                // eslint-disable-next-line security/detect-object-injection
                followStates[followKey],
                leadState,
            )) {
                sink.push(event);
            }
        };

        /*
        read lead query events and pass to sink, also end sink when this function ends
        */
        const emitEventsFromLeadQuery = async () => {
            for (const followKey of new Set(getFollowKeys(leadState))) {
                emitEventsFromFollowQuery(followKey).
                    catch(error => sink.error(error));
            }

            const { signal } = leadAbortController;

            try {
                for await (
                    const { event: leadEvent, state: leadStateNext } of leadEventIterable
                ) {
                    const leadStatePrev = leadState;
                    leadState = leadStateNext;

                    for (const event of mapLeadEvent(
                        leadEvent,
                        leadStateNext, leadStatePrev,
                        followStates,
                    )) {
                        sink.push(event);
                    }

                    const followKeysPrev = new Set(getFollowKeys(leadStatePrev));
                    const followKeysNext = new Set(getFollowKeys(leadStateNext));

                    const followKeysToAbort = Array.from(followKeysPrev).
                        filter(followKeyPrev => !followKeysNext.has(followKeyPrev));
                    const followKeysToSpawn = Array.from(followKeysNext).
                        filter(followKeyNext => !followKeysPrev.has(followKeyNext));

                    for (const followKey of followKeysToAbort) {
                        assert(followKey in followAbortControllers);
                        assert(followKey in followQueryPromises);

                        // eslint-disable-next-line security/detect-object-injection
                        const followAbortController = followAbortControllers[followKey];
                        // eslint-disable-next-line security/detect-object-injection
                        const followQueryPromise = followQueryPromises[followKey];

                        followAbortController.abort();
                        followQueryFactory.release(followQueryPromise);

                        // eslint-disable-next-line security/detect-object-injection
                        delete followAbortControllers[followKey];
                        // eslint-disable-next-line security/detect-object-injection
                        delete followQueryPromises[followKey];
                    }
                    for (const followKey of followKeysToSpawn) {
                        assert(!(followKey in followAbortControllers));
                        assert(!(followKey in followQueryPromises));

                        const followAbortController = new AbortController();
                        setupCascadingAbort(signal, followAbortController);
                        const followArgs = getFollowArgs(leadStateNext, followKey);
                        const followQueryPromise = followQueryFactory.acquire(...followArgs);

                        // eslint-disable-next-line security/detect-object-injection
                        followAbortControllers[followKey] = followAbortController;
                        // eslint-disable-next-line security/detect-object-injection
                        followQueryPromises[followKey] = followQueryPromise;
                    }
                    await Promise.all(followKeysToSpawn.map(resolveFollowQuery));

                    for (const followKey of followKeysToSpawn) {
                        emitSnapshotFromFollowQuery(followKey);
                    }

                    for (const followKey of followKeysToSpawn) {
                        emitEventsFromFollowQuery(followKey).
                            catch(error => sink.error(error));
                    }

                }
            }
            catch (error) {
                if (!signal.aborted) throw error;
            }

            sink.done();
        };

        /*
        read follow query events and pass to sink
        */
        const emitEventsFromFollowQuery = async (followKey: FollowKey) => {
            // eslint-disable-next-line security/detect-object-injection
            const { signal } = followAbortControllers[followKey];

            try {
                for await (
                    const { event: followEvent, state: followStateNext } of
                    // eslint-disable-next-line security/detect-object-injection
                    followEventIterables[followKey]
                ) {
                    // eslint-disable-next-line security/detect-object-injection
                    const followStatePrev = followStates[followKey];
                    followStates = { ...followStates };
                    // eslint-disable-next-line security/detect-object-injection
                    followStates[followKey] = followStateNext;

                    for (const event of mapFollowEvent(
                        followEvent,
                        followKey,
                        followStateNext, followStatePrev,
                        leadState,
                    )) {
                        sink.push(event);
                    }
                }
            }
            catch (error) {
                if (!signal.aborted) throw error;
            }

        };

        //#endregion

        /*
        setup everything
        */
        const leadQuery = await Promise.race([
            leadQueryPromise,
            waitForAbort(leadAbortController.signal, "lead-follow aborted"),
        ]);
        let leadState = leadQuery.getState();
        const leadEventIterable = leadQuery.fork(leadAbortController.signal);

        let followStates = {} as Record<
            FollowKey,
            FollowState
        >;
        const followEventIterables = {} as Record<
            FollowKey,
            AsyncIterable<QueryElement<FollowState, FollowEvent>>
        >;

        /*
        We load the initial state
        */
        await initializeState();

        assertAbortSignal(signal, "lead-follow aborted");

        /*
        all events are emitted via sink
        */
        const sink = createBufferedIterable<E>();

        /*
         First we emit snapshots!
         */
        emitSnapshotFromLeadQuery();
        /*
        and then all the events
        */
        emitEventsFromLeadQuery().
            catch(error => sink.error(error));

        yield* sink;

        assertAbortSignal(signal, "lead-follow aborted");
    }
    finally {
        /*
        abort all follow queries
        */
        for (
            const followAbortController of
            Object.values<AbortController>(followAbortControllers)
        ) {
            followAbortController.abort();
        }
        /*
        release all follow queries
        */
        for (
            const followQueryPromise of
            Object.values<Promise<Query<FollowState, FollowEvent>>>(followQueryPromises)
        ) {
            followQueryFactory.release(followQueryPromise);
        }

        /*
        abort lead query
        */
        leadAbortController.abort();
        /*
        release lead query query
        */
        leadQueryFactory.release(leadQueryPromise);
    }

}
